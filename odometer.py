#!usr/bin/python
import sys

def possible_readings(digit):
	from itertools import combinations
	possible = [int(''.join(map(str,l))) for l in combinations(range(1,10),digit)]
	return possible

def valid_pos(current_reading,readings):
	return readings.index(current_reading) if current_reading in readings else None
	

def increment(position,increment_for,readings):
	expected_readings = []
	count = 0
	end = position + increment_for
	while position != end:
		if position == len(readings):
                        position = 0
                        end = increment_for - count 
		expected_readings.append(readings[position])
		position += 1
		count += 1
	return expected_readings

print "Enter the size:"
digit = int(raw_input());
if digit > 1 and digit < 9:
	readings = possible_readings(digit)
	print "minimum reading :",readings[0]
	print "maximum reading :",readings[len(readings)-1]
	print "enter a",digit,"digit reading:"
	current_reading = int(raw_input());
	if valid_pos(current_reading,readings) == None:
		print "Not valid"
		exit()	
	else:
		print "Reading incremented for:"
		increment_for = int(raw_input());
		starting_from = valid_pos(current_reading,readings) + 1
		print "Next Reading/Readings:"
		for each_reading in increment(starting_from,increment_for,readings):
			print each_reading
else:
	print "Only lenght of 2-8 is accepted"
