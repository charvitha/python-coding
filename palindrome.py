def substrings(string):
	length = len(string)
	return set(string[i:j+1] for i in range(length) for j in range(i,length))
			
def is_palindrome(string):
	return string == string[::-1]


palindromes=[]
list_substrings = substrings(raw_input())
for string in list_substrings :
	if(is_palindrome(string)):
		palindromes.append(string)
print max(palindromes, key=len)
